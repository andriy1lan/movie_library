The Movie Library Project is the implementation of Film Catalogue, using ASP.NET MVC 4 (Razor), Entity Framework, MS SQL Server. 
The Library allows to get all movies, search a movie in titles 
and countries, as well as it allows to add new movie, update
movie info, and delete movie from library.
For Unit testing it uses MS Tests and Moq.
The Project is composed with 3 parts (Controllers):
CRUD movies operations and rendering separate Views, SPA Movie View, that exploits Partial Views and jQuery for subviews manipulations
(inserting, updating, deleting), and the Authentication part,
that allows Registration, Login, and Logout of Users
in Library, so main movie actions are authorized.
Log4Net logger with ConsoleOutput and File Appenders
are applied for Authentication.
This project is throughout and extended  modification of my initial MovieLibrary project, written once, that used ADO.NET as DAL.
  