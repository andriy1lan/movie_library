﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieLib.Models
{
    public class Movie
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        [StringLength(60, MinimumLength = 3)]
        [Required(ErrorMessage = "Put the name of film")]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "Put the origin of film")]
        public string Country { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ReleaseDate { get; set; }

        public Movie(string Name, string Country, DateTime ReleaseDate)
        {
            this.Name = Name;
            this.Country = Country;
            this.ReleaseDate = ReleaseDate;
        }

        public Movie(int Id,string Name, string Country, DateTime ReleaseDate)
        {
            this.Id = Id;
            this.Name = Name;
            this.Country = Country;
            this.ReleaseDate = ReleaseDate;
        }

        public Movie() { }
    }
}