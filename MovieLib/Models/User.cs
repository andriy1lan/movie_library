﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieLib.Models
{
    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Write the Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Write the Username")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Write the Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm the Password")]
        [Compare("Password", ErrorMessage = "Passwords do not match")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Write the Email")]
        [EmailAddress(ErrorMessage = "Incorrect email address.")]
        public string Email { get; set; }
    }
}