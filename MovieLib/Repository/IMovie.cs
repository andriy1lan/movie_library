﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MovieLib.Models;

namespace MovieLib.Repository
{
    public interface IMovie
    {
        void AddMovie(Movie item);
        IEnumerable<Movie> GetMovies();
        IEnumerable<Movie> GetMovies(String s);
        Movie GetMovie(int id);
        void UpdateMovie(Movie item);
        void DeleteMovie(int id);
    }
}
